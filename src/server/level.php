<?php

header('Content-type: application/xml');
header('Access-Control-Allow-Origin: *');


$action = $_POST["action"];
$name = $_POST["name"];
$isBinary = $_POST["isBinary"];

switch($action)
{
	case "save":
		{
		if(!is_dir("map")) {
			mkdir("map");
		}

		$terrain = $_POST["terrain"];
		$entity = $_POST["entity"];

		$isOffline = (int)$_POST["isOffline"];

		if(!$isOffline)
		{
			$file = fopen("map/{$name}_t.map", "wb");
			fwrite($file, $terrain);

			$file = fopen("map/{$name}_e.map", "wb");
			fwrite($file, $entity);
		}
		else
		{
			@mkdir("map/offline");

			$file = fopen("map/offline/{$name}.js", "wb");

			$level = array();
			$level["name"] = $name;
			$level["terrain"] = $terrain;
			$level["entity"] = $entity;
			$level = json_encode($level);

			$data = "Map.level_{$name} = {$level}";
			fwrite($file, $data);
		}
		} break;

	case "load":
		{
		$return = array();
		$return["name"] = $name;
		//$return["data"] = file_get_contents($name);
		$return["terrain"] = GetMapData("map/{$name}_t.map");
		$return["entity"] = GetMapData("map/{$name}_e.map");;
		echo json_encode($return);
		} break;
}

die();

function GetMapData($fileName)
{
	$file = @fopen($fileName, "rb");
	if($file === false) {
		ThrowNotFound();
	}

	$size = filesize($fileName);

	if($size > 0) {
		return fread($file, $size);
	}

	ThrowNotFound();

	return "";
}

function ThrowNotFound()
{
	header("HTTP/1.0 418 I'm a teapot");
	header("Status 418 I'm a teapot");
	die();
}
