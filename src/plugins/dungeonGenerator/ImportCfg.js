Define("DungeonGenerator", "Event", [
	"GENERATE", "SPAWN_PLAYER", "GET_TRIGGER", "GENERATE_HD"
]);

Define("DungeonGenerator", "Type", [
	"MAIN_ROOM", "ROOM", "CORRIDOR"
]);