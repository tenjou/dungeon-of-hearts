"use strict";

DungeonGenerator.Manager = Plugin.extend
({
	init: function()
	{
		var self = this;
		this.chn_dungeonGenerator = SubscribeChannel(this, "DungeonGenerator", function(event, data) {
			return self.handleSignal_DungeonGenerator(event, data);
		});

		this.triggers = [];
	},

	install: function() {
		this._loadBrushes();
	},

	clear: function() {
		this._info = null;
		this._map = null;
		this._sizeX = this._sizeY = 0;
		this.triggers.length = 0;
	},


	generate: function(info)
	{
		this._info = info;

		this._sizeX = info.sizeX;
		this._sizeY = info.sizeY;
		this._maxSizeX = Terrain.Cfg.sizeX;
		this._maxSizeY = Terrain.Cfg.sizeY;

		// Adjust size of the map.
		if(this._width < 3) 				{ this._width = 3; }
		if(this._height < 3) 				{ this._height = 3; }
		if(this._width > this._maxSizeX) 	{ this._width = this._maxSizeX; }
		if(this._height > this._maxSizeY) 	{ this._height = this._maxSizeY; }

		// Initialize basic map.
		this._map = new Array(this._sizeX * this._sizeY);
		this._features = [];
		var index;

		for(var y = 0; y < this._sizeY; y++)
		{
			for(var x = 0; x < this._sizeX; x++)
			{
				index = x + (y * this._sizeX);

				if(y === 0) 					{ this._map[index] = this.brush_stoneWall; }
				else if(x === 0) 				{ this._map[index] = this.brush_stoneWall; }
				else if(y === this._sizeY-1) 	{ this._map[index] = this.brush_stoneWall; }
				else if(x === this._sizeX-1) 	{ this._map[index] = this.brush_stoneWall; }
				else 							{ this._map[index] = this.brush_ground; }
			}
		}

		// Initialize starting room.
		this._makeRoom(this._sizeX / 2, this._sizeY / 2, 10, 10, Random.getNumber(0, 3));

		// Generate the map.
		var numCurrFeatures = 0;

		for(var tries = 0; tries < 100; tries++)
		{
			numCurrFeatures = this._features.length;

			// Check if we reached our goal.
			if(numCurrFeatures === this._info.numFeatures) {
				break;
			}

			// Pick random feature.
			var featureID = Random.getNumber(1, numCurrFeatures) - 1;
			var feature = this._features[featureID];

			// Pick random wall of the feature.
			var wallDirection = Random.getNumber(0, 3);
			var cursorX = 0;
			var cursorY = 0;

			switch(wallDirection)
			{
				case 0: // NORTH
				{
					cursorX = Random.getNumber(feature.minX+1, feature.maxX-2);
					cursorY = feature.minY;
				} break;

				case 1: // SOUTH
				{
					cursorX = Random.getNumber(feature.minX+1, feature.maxX-2);
					cursorY = feature.maxY - 1;
				} break;

				case 2: // WEST
				{
					cursorX = feature.minX;
					cursorY = Random.getNumber(feature.minY+1, feature.maxY-2);
				} break;

				case 3: // EAST
				{
					cursorX = feature.maxX - 1;
					cursorY = Random.getNumber(feature.minY+1, feature.maxY-2);
				} break;
			}

			var chance = Random.getNumber(0, 99);
			if(chance < 25) // 25% that there will be a corridor
			{
				if(this._makeCorridor(cursorX, cursorY, 9, wallDirection)) {
					this._map[cursorX + (cursorY * this._sizeX)] = this.brush_roomTrigger;
					this.triggers.push(new DungeonGenerator.Trigger(cursorX, cursorY));
				}
			}
			else
			{
				if(this._makeRoom(cursorX, cursorY, 10, 10, wallDirection)) {
					this._map[cursorX + (cursorY * this._sizeX)] = this.brush_roomTrigger;
					this.triggers.push(new DungeonGenerator.Trigger(cursorX, cursorY));
				}
			}
		}

		this._linkTriggers();

		//
		var map = this._map;

		return map;
	},

	generateHD: function()
	{
		var terrainGrid = Terrain.Cfg.grid;
		var itemID, tempItemID, index, tempIndex;
		var itemX, itemY;
		var maxIndex = (this._sizeX * this._sizeY) - 1;

		for(var y = 0; y < this._sizeY; y++)
		{
			for(var x = 0; x < this._sizeX; x++)
			{
				index = x + (y * this._sizeX);
				itemID = this._map[index];

				// Drop shadow
				if(itemID === this.brush_floor || itemID === this.brush_npcSpawn || itemID === this.brush_roomTrigger)
				{
					tempIndex = index - this._sizeX;
					if(tempIndex < 0) { continue;}

					tempItemID = this._map[tempIndex];
					if(tempItemID === this.brush_wall)
					{
						itemX = x * terrainGrid.width;
						itemY = y * terrainGrid.height;
						mighty.Macro.CreateEntity("shadow", itemX, itemY);
					}
				}
				else if(itemID === this.brush_wall)
				{
					tempIndex = index + this._sizeX;
					if(tempIndex > maxIndex) { continue; }

					tempItemID = this._map[tempIndex];
					if(tempItemID === -1)
					{
						itemX = x * terrainGrid.width;
						itemY = y * terrainGrid.height;
						mighty.Macro.CreateEntity("blockShadow", itemX, itemY);
					}
				}
			}
		}

		return null;
	},


	_loadBrushes: function() {
		this.brush_floor = Palettes.Entity.getByName("floor").id;
		this.brush_debug = Palettes.Entity.getByName("debug").id;
		this.brush_ground = Palettes.Entity.getByName("ground").id;
		this.brush_wall = Palettes.Entity.getByName("wall").id;
		this.brush_stoneWall = Palettes.Entity.getByName("stoneWall").id;
		this.brush_roomTrigger = Palettes.Entity.getByName("roomTrigger").id;
		this.brush_npcSpawn = Palettes.Entity.getByName("npcSpawn").id;

		this.brush_stoneWall = -1;
		this.brush_ground = -1;

		this.brush_shadow = Palettes.Entity.getByName("shadow").id;
	},

	_makeRoom: function(startX, startY, sizeX, sizeY, direction)
	{
		// Randomize size of the room, it should be at least 4x4 tiles.
		var roomSizeX = Random.getNumber(4, sizeX);
		var roomSizeY = Random.getNumber(4, sizeY);

		// Try to build a room.
		var roomX, roomY;

		switch(direction)
		{
			case 0: // NORTH
			{
				roomX = startX - Math.floor(roomSizeX / 2);
				roomY = startY - roomSizeY + 1;

				if(!this._isAreaFree(roomX, roomY, roomSizeX, roomSizeY)) { return false; }
				this._buildArea(roomX, roomY, roomSizeX, roomSizeY);
			} break;

			case 1: // SOUTH
			{
				roomX = startX - Math.floor(roomSizeX / 2);
				roomY = startY;

				if(!this._isAreaFree(roomX, roomY, roomSizeX, roomSizeY)) { return false; }
				this._buildArea(roomX, roomY, roomSizeX, roomSizeY);
			} break;

			case 2: // WEST
			{
				roomX = startX - roomSizeX + 1;
				roomY = startY - Math.floor(roomSizeY / 2);

				if(!this._isAreaFree(roomX, roomY, roomSizeX, roomSizeY)) { return false; }
				this._buildArea(roomX, roomY, roomSizeX, roomSizeY);
			} break;

			case 3: // EAST
			{
				roomX = startX;
				roomY = startY - Math.floor(roomSizeY / 2);

				if(!this._isAreaFree(roomX, roomY, roomSizeX, roomSizeY)) { return false; }
				this._buildArea(roomX, roomY, roomSizeX, roomSizeY);
			} break;
		}

		return true;
	},

	_makeCorridor: function(startX, startY, length, direction)
	{
		var corridorLength = Random.getNumber(6, length);

		// Try to build a corridor.
		var roomX, roomY;

		switch(direction)
		{
			case 0: // NORTH
			{
				roomX = startX - 1;
				roomY = startY - corridorLength + 1;

				if(!this._isAreaFree(roomX, roomY, 3, corridorLength)) { return false; }
				this._buildArea(roomX, roomY, 3, corridorLength);
			} break;


			case 1: // SOUTH
			{
				roomX = startX - 1;
				roomY = startY;

				if(!this._isAreaFree(roomX, roomY, 3, corridorLength)) { return false; }
				this._buildArea(roomX, roomY, 3, corridorLength);
			} break;

			case 2: // WEST
			{
				roomX = startX - corridorLength + 1;
				roomY = startY - 1;

				if(!this._isAreaFree(roomX, roomY, corridorLength, 3)) { return false; }
				this._buildArea(roomX, roomY, corridorLength, 3);
			} break;

			case 3: // EAST
			{
				roomX = startX;
				roomY = startY - 1;

				if(!this._isAreaFree(roomX, roomY, corridorLength, 3)) { return false; }
				this._buildArea(roomX, roomY, corridorLength, 3);
			} break;
		}

		return true;
	},


	_buildArea: function(mapX, mapY, sizeX, sizeY)
	{
		// Calculate optimized mapMax size (-1) so we don't have to do it in the loop.
		var mapMaxX = mapX + sizeX - 1;
		var mapMaxY = mapY + sizeY - 1;
		var index;

		for(var y = mapY; y <= mapMaxY; y++)
		{
			for(var x = mapX; x <= mapMaxX; x++)
			{
				index = x + (y * this._sizeX);

				if(x === mapX) 			{ this._map[index] = this.brush_wall; }
				else if(y === mapY) 	{ this._map[index] = this.brush_wall; }
				else if(x === mapMaxX)	{ this._map[index] = this.brush_wall; }
				else if(y === mapMaxY)	{ this._map[index] = this.brush_wall; }
				else					{ this._map[index] = this.brush_floor; }
			}
		}

		// If spawn is not generated, generate one.
		if(!this.isSpawn)
		{
			this.spawnX = mapX + Math.floor((mapMaxX - mapX) / 2);
			this.spawnY = mapY + Math.floor((mapMaxY - mapY) / 2);
			this._map[this.spawnX + (this.spawnY * this._sizeX)] = Palettes.Entity.getByName("spawn").id;
			this.isSpawn = true;
			this.triggers.push(new DungeonGenerator.Trigger(this.spawnX, this.spawnY));

			var npcSpawnX = mapX + 1;
			var npcSpawnY = mapY + 1;
			this._map[npcSpawnX + (npcSpawnY * this._sizeX)] = this.brush_npcSpawn;

//			var npcSpawnX = mapX + 2;
//			var npcSpawnY = mapY + 2;
//			this._map[npcSpawnX + (npcSpawnY * this._sizeX)] = this.brush_npcSpawn;
		}

		this._features.push(new DungeonGenerator.Feature(mapX, mapY, sizeX, sizeY));
	},

	_isAreaFree: function(mapX, mapY, sizeX, sizeY)
	{
		var mapMaxX = mapX + sizeX;
		var mapMaxY = mapY + sizeY;
		var index, brushID;

		for(var y = mapY; y < mapMaxY; y++)
		{
			if(y < 0 || y > this._maxSizeY) {
				return false;
			}

			for(var x = mapX; x < mapMaxX; x++)
			{
				if(x < 0 || x > this._maxSizeX) {
					return false;
				}

				index = x + (y * this._sizeX);
				brushID = this._map[index];

				if(brushID !== this.brush_ground && brushID !== this.brush_wall) {
					return false;
				}
			}
		}

		return true;
	},

	_linkTriggers: function()
	{
		var trigger, feature, n;
		var numTriggers = this.triggers.length;
		var numFeatures = this._features.length;

		for(var i = 0; i < numTriggers; i++)
		{
			trigger = this.triggers[i];

			for(n = 0; n < numFeatures; n++)
			{
				feature = this._features[n];

				if(trigger.x >= feature.minX && trigger.x <= feature.maxX &&
				   trigger.y >= feature.minY && trigger.y <= feature.maxY)
				{
					trigger.buffer.push(feature);
				}
			}
		}
	},


	handleSignal_DungeonGenerator: function(event, data)
	{
		switch(event)
		{
			case DungeonGenerator.Event.GET_TRIGGER:
				return this.getTrigger(data.gridX, data.gridY);

			case DungeonGenerator.Event.GENERATE:
				return this.generate(data);
			case DungeonGenerator.Event.GENERATE_HD:
				return this.generateHD();

			case DungeonGenerator.Event.SPAWN_PLAYER:
				return this.spawnPlayer();

			case DungeonGenerator.Event.GET_SPAWNS:
				return this.npcSpawns;
		}
	},


	spawnPlayer: function()
	{
		if(!this.isSpawn) { return; }

		var x = this.spawnX * Terrain.Cfg.grid.width;
		var y = this.spawnY * Terrain.Cfg.grid.height;
		return mighty.Macro.CreateEntity("player", x, y);
	},

	getTrigger: function(gridX, gridY)
	{
		var trigger;
		var numTriggers = this.triggers.length;

		for(var i = 0; i < numTriggers; i++)
		{
			trigger = this.triggers[i];
			if(trigger.x === gridX && trigger.y === gridY) {
				return trigger;
			}
		}

		return null;
	},


	//
	chn_dungeonGenerator: null,

	brush_debug: -1,
	brush_floor: -1,
	brush_ground: -1,
	brush_wall: -1,
	brush_stoneWall: -1,
	brush_roomTrigger: -1,
	brush_npcSpawn: -1,
	brush_shadow: -1,

	isSpawn: false,
	spawnX: 0,
	spawnY: 0,

	_info: null,
	_map: null,
	_features: null,
	_sizeX: 0, _sizeY: 0,
	_maxSizeX: 0, _maxSizeY: 0,

	triggers: null
});

DungeonGenerator.Info = function(sizeX, sizeY) {
	this.sizeX = sizeX;
	this.sizeY = sizeY;
	this.numFeatures = 0;
};

DungeonGenerator.Feature = function(x, y, sizeX, sizeY) {
	this.minX = x;
	this.minY = y;
	this.maxX = x + sizeX;
	this.maxY = y + sizeY;
	this.isVisible = false;
	this.type = -1;
};

DungeonGenerator.Trigger = function(x, y) {
	this.x = x;
	this.y = y;
	this.buffer = [];
};