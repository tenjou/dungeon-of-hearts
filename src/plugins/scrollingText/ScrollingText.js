ScrollingText.Manager = Plugin.extend
({
	install: function()
	{
		this.texts = [];

		var self = this;
		SubscribeChannel(this, "ScrollingText", function(event, data) {
			self.handleSignal_ScrollingText(event, data);
		});
	},

	clear: function() {
		mighty.Templates.scrollingText.js.clear();
	},

	handleSignal_ScrollingText: function(event, data)
	{
		var element, element2;

		switch(event)
		{
			case ScrollingText.Event.ADD:
			{
				element = mighty.Templates.scrollingText.js.add(data.text, data.x, data.y);
				this.addTimer(function() {
					mighty.Templates.scrollingText.js.remove(element);
				}, 500, 1);
			} break;

			case ScrollingText.Event.ADD_CRITICAL:
			{
				element2 = mighty.Templates.scrollingText.js.addCritical(data.x-30, data.y);
				this.addTimer(function() {
					mighty.Templates.scrollingText.js.remove(element2);
				}, 1000, 1);

				element = mighty.Templates.scrollingText.js.add(data.text, data.x, data.y);
				this.addTimer(function() {
					mighty.Templates.scrollingText.js.remove(element);
				}, 1000, 1);
			} break;
		}
	},


	//
	texts: null
});

ScrollingText.Info = {
	text: "",
	x: 0,
	y: 0
};