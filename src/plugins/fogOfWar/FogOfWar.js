FogOfWar.Manager = Plugin.extend
({
	install: function()
	{
		var self = this;
		SubscribeChannel(this, "FogOfWar", function(event, data) {
			self.handleSignal_FogOfWar(event, data);
		});
	},


	handleSignal_FogOfWar: function(event, data)
	{
		switch(event)
		{
			case FogOfWar.Event.LIGHT_FEATURE:
				this.lightFeature(data);
				break;
		}
	},


	lightFeature: function(data)
	{
		if(data.isVisible) { return; }

		// Lazy way for a need for speed.
		var grid = Patch.plugin.grid;
		var minX = data.minX;
		var maxX = data.maxX;
		var minY = data.minY;
		var maxY = data.maxY;

		var items, numItems, i, entity;
		for(var y = minY; y < maxY; y++)
		{
			for(var x = minX; x < maxX; x++)
			{
				items = grid.getCellAt(x, y);
				if(items === void(0)) { continue; }

				if(items)
				{
					numItems = items.length;
					for(i = 0; i < numItems; i++) {
						entity = items[i];
						entity.setVisible(true);
					}
				}
			}
		}
	}
});