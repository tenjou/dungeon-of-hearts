AI.AStarBuffer = function(itemFunc)
{
	this.push = function(node)
	{
		var prevNode = null;
		var currNode = this.firstNode;

		do
		{
			if(node.h < currNode.h)
			{
				node.prev = prevNode;
				node.next = currNode;
				currNode.prev = node;
				prevNode.next = node;
				this.length++;

				return;
			}

			prevNode = currNode;
			currNode = currNode.next;
		} while(currNode);

		console.log("Error: DEPTH LIST - OUT OF BONDS");
	};


	this.popFront = function()
	{
		if(this.firstNode.next === this.lastNode) { return null; }

		var nextNode = this.firstNode.next;
		nextNode.prev.next = nextNode.next;
		nextNode.next.prev = nextNode.prev;
		this.length--;

		return nextNode;
	};

	this.popBack = function()
	{
		if(this.lastNode === null) { return null; }

		var prevNode = this.lastNode.prev;
		prevNode.next = null;
		this.lastNode = prevNode;

		this.length--;

		return this.lastNode;
	};


	this.update = function(node)
	{
		// Try to sink down.
		var currNode = node.prev;
		if(currNode !== this.firstNode && currNode.depth > node.depth)
		{
			node.next.prev = node.prev;
			node.prev.next = node.next;

			do
			{
				if(currNode.depth < node.depth)
				{
					node.prev = currNode;
					node.next = currNode.next;
					node.next.prev = node;
					currNode.next = node;
					return;
				}

				currNode = currNode.prev;
			} while(currNode);
		}

		// Try to bubble up.
		currNode = node.next;
		if(currNode !== this.lastNode && currNode.depth < node.depth)
		{
			node.next.prev = node.prev;
			node.prev.next = node.next;

			do
			{
				if(currNode.depth > node.depth)
				{
					node.next = currNode;
					node.prev = currNode.prev;
					node.prev.next = node;
					currNode.prev = node;
					return;
				}

				currNode = currNode.next;
			} while(currNode);
		}
	};


	this.print = function()
	{
		var currNode = this.firstNode;

		do
		{
			console.log(currNode.f);
			currNode = currNode.next;
		} while(currNode)
	};


	//
	this.length = 0;
	this.firstNode = new AI.AStarNode(null);
	this.lastNode = new AI.AStarNode(null);

	this.firstNode.h = -2147483648;
	this.firstNode.next = this.lastNode;
	this.lastNode.h = 2147483648;
	this.lastNode.prev = this.firstNode;
};