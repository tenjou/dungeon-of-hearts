AI.AStarNode = function(cell, x, y)
{
	this.clear = function()
	{
		this.next = null;
		this.prev = null;
		this.parent = null;

		this.g = 0;
		this.h = 0;
		this.f = 0;

		this.isVisited = false;
		this.isClosed = false;
	};

	this.isDistance = function(node, length)
	{
		if(Math.abs(this.x - node.x) >= length) { return false; }
		if(Math.abs(this.y - node.y) >= length) { return false; }

		return true;
	};


	//
	this.next = null;
	this.prev = null;

	this.cell = cell;
	this.x = x;
	this.y = y;

	this.parent = null;
	this.g = 0;
	this.h = 0;
	this.f = 0;

	this.isVisited = false;
	this.isClosed = false;
};