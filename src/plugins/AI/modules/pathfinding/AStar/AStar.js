"use strict";

AI.AStar = Class.extend
({
	init: function() {
		this.neighbors = new Array(4);
	},

	loadNodes: function(grid)
	{
		this.sizeX = Terrain.Cfg.sizeX;
		this.sizeY = Terrain.Cfg.sizeY;

		var index;
		this.nodes = new Array(grid.length);

		for(var y = 0; y < this.sizeY; y++)
		{
			for(var x = 0; x < this.sizeX; x++)
			{
				index = x + (y * this.sizeX);
				this.nodes[index] = new AI.AStarNode(grid[index], x, y);
			}
		}
	},

	clearNodes: function()
	{
		var node;
		var numNodes = this.nodes.length;

		for(var i = 0; i < numNodes; ++i)
		{
			node = this.nodes[i];
			if(node) {
				node.clear();
			}
		}
	},


	loadNeighbors: function(node, targetX, targetY)
	{
		this.numNeighbors = 0;
		var x = node.x;
		var y = node.y;
		var isMoveAble;

		// LEFT
		var leftNode = this.getNodeAt(x-1, y);
		isMoveAble = this.isNodeWalkable(leftNode);
		if(leftNode && !leftNode.isClosed && isMoveAble) {
			this.neighbors[0] = leftNode;
			this.numNeighbors++;
		}

		// TOP
		var topNode = this.getNodeAt(x, y-1);
		isMoveAble = this.isNodeWalkable(topNode);
		if(topNode && !topNode.isClosed && isMoveAble) {
			this.neighbors[this.numNeighbors] = topNode;
			this.numNeighbors++;
		}

		// RIGHT
		var rightNode = this.getNodeAt(x+1, y);
		isMoveAble = this.isNodeWalkable(rightNode);
		if(rightNode && !rightNode.isClosed && isMoveAble) {
			this.neighbors[this.numNeighbors] = rightNode;
			this.numNeighbors++;
		}

		// BOTTOM
		var bottomNode = this.getNodeAt(x, y+1);
		isMoveAble = this.isNodeWalkable(bottomNode);
		if(bottomNode && !bottomNode.isClosed && isMoveAble) {
			this.neighbors[this.numNeighbors] = bottomNode;
			this.numNeighbors++;
		}
	},


	search: function(startX, startY, targetX, targetY, heuristicType)
	{
		this.clearNodes();

		var open = new AI.AStarBuffer();
		var heuristicFunc = this.getHeuristic(heuristicType);

		var currNode = null;
		var startNode = this.getNodeAt(startX, startY);
		var endNode = this.getNodeAt(targetX, targetY);
		open.push(startNode);

		while(open.length > 0)
		{
			currNode = open.popFront();

			// Result has been found.
			if(currNode === endNode)
			{
				var node = currNode;
				var path = new Array();

				while(node.parent) {
					path.push(node);
					node = node.parent;
				}

				return path;
			}

//			if(currNode.isDistance(endNode, 1))
//			{
//				var node = currNode;
//				var path = new Array();
//
//				while(node.parent) {
//					path.push(node);
//					node = node.parent;
//				}
//
//				return path;
//			}

			currNode.isClosed = true;
			this.loadNeighbors(currNode, targetX, targetY);

			for(var i = 0; i < this.numNeighbors; ++i)
			{
				var neighbor = this.neighbors[i];
				var g = currNode.g + 1;
				var wasVisited = neighbor.isVisited;

				if(!wasVisited || neighbor.g < g)
				{
					neighbor.parent = currNode;
					neighbor.h = neighbor.h || heuristicFunc(neighbor, endNode);
					neighbor.g = g;
					neighbor.f = neighbor.g + neighbor.h;
					neighbor.isVisited = true;

					if(!wasVisited) {
						open.push(neighbor);
					}
					else {
						open.update(neighbor);
					}
				}
			}
		}

		return [];
	},


	isNodeWalkable: function(node)
	{
		if(!node) { return; }

		var items = node.cell;
		if(!items || !items.length) { return true; }

		var numItems = items.length;

		for(var i = 0; i < numItems; i++) {
			if(items[i].component.Pathfinding) { return false;}
		}

		return true;
	},


	getHeuristic: function(type)
	{
		if(type === void(0)) {
			type = AI.HeuristicType.MANHATTAN;
		}

		switch(type)
		{
			case AI.HeuristicType.EUCLIDIAN:
				return AI.Heuristic.euclidian;

			case AI.HeuristicType.DIAGONAL:
				return AI.Heuristic.diagonal;

			default:
			case AI.HeuristicType.MANHATTAN:
				return AI.Heuristic.manhattan;
		}
	},

	getNodeAt: function(x, y)
	{
		if(x < 0 || x >= this.sizeX) { return null; }
		if(y < 0 || y >= this.sizeY) { return null; }

		return this.nodes[x + (y * this.sizeX)];
	},


	//
	nodes: null,
	numNodes: null,

	neighbors: null,
	numNeighbors: 0,

	sizeX: 0,
	sizeY: 0
});
