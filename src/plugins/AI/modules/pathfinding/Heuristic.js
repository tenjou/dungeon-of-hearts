AI.Heuristic =
{
	manhattan: function(node, destNode, cost)
	{
		var dx = Math.abs(node.x - destNode.x);
		var dy = Math.abs(node.y - destNode.y);

		return dx + dy;
	},

	euclidian: function(node, destNode, cost)
	{
		var dx = node.x - destNode.x;
		var dy = node.y - destNode.y;

		return Math.sqrt(dx * dx + dy * dy);
	},

	diagonal: function(node, destNode, cost, diagonalCost)
	{
		var dx = Math.abs(node.x - destNode.x);
		var dy = Math.abs(node.y - destNode.y);

		var diagonal = Math.min(dx, dy);
		var straight = dx + dy;

		return (diagonalCost * diagonal) + (cost * (straight - 2 * diagonal));
	}
};