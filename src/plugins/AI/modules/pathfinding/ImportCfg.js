Define("Pathfinding", "Event", [
	"GET_PATH"
]);

Define("AI", "HeuristicType", [
	"MANHATTAN", "EUCLIDIAN", "DIAGONAL"
]);