"use strict";

mighty.CreateModule("AI", "Pathfinding",
{
	init: function()
	{
		this.aStar = new AI.AStar();

		var self = this;
		SubscribeChannel(this, "Pathfinding", function(event, data) {
			return self.handleSignal_Pathfinding(event, data);
		});
	},

	load: function() {
		this.aStar.loadNodes(Patch.plugin.grid.cells);
	},

	handleSignal_Pathfinding: function(event, data)
	{
		switch(event)
		{
			case Pathfinding.Event.GET_PATH:
				return this.aStar.search(data.startX, data.startY, data.endX, data.endY);
		}
	},


	//
	aStar: null
});

AI.PathfindingInfo = function(startX, startY, endX, endY) {
	this.startX = startX;
	this.startY = startY;
	this.endX = endX;
	this.endY = endY;
};