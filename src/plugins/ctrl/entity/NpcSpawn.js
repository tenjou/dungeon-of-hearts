Entity.NpcSpawn = Entity.Geometry.extend
({
	setVisible: function(value)
	{
		this.isVisible = true;

		if(!this.isSpawned) {
			SendSignal("Npc", Npc.Event.SPAWN, this);
			this.isSpawned = true;
		}
	},

	//
	isSpawned: false
});