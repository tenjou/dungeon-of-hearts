"use strict";

Entity.Player = Entity.Character.extend
({
	activate: function()
	{
		this._super();

		this.cellInfo = new Patch.CellInfo(0, 0);
		this.updateTrigger();
	},


	updateTrigger: function()
	{
		this.cellInfo.gridX = this.gridX;
		this.cellInfo.gridY = this.gridY;
		var trigger = AskData("DungeonGenerator", DungeonGenerator.Event.GET_TRIGGER, this.cellInfo);

		var feature;
		var features = trigger.buffer;
		var numFeatures = features.length;

		for(var i = 0; i < numFeatures; i++)
		{
			feature = features[i];
			if(feature.isVisible) { return; }
			SendSignal("FogOfWar", FogOfWar.Event.LIGHT_FEATURE, feature);
		}
	},


	doTurn: function(direction)
	{
		if(direction !== Direction.NONE)
		{
			var nextGridX = this.gridX;
			var nextGridY = this.gridY;

			switch(direction)
			{
				case Direction.LEFT:
					nextGridX--;
					break;

				case Direction.RIGHT:
					nextGridX++;
					break;

				case Direction.UP:
					nextGridY--;
					break;

				case Direction.DOWN:
					nextGridY++;
					break;
			}

			this.cellInfo.gridX = nextGridX;
			this.cellInfo.gridY = nextGridY;
			var items = AskData("Grid", Grid.Event.GET_CELL, this.cellInfo);

			var canMove = true;
			var updateTrigger = false;

			if(items && items.length)
			{
				var numItems = items.length;
				var item;

				for(var i = 0; i < numItems; i++)
				{
					item = items[i];
					if(item.type === GameObject.WALL) {
						return; // Cant move and turn doesnt happen!
					}
					else if(item.type === GameObject.NPC) {
						canMove = false;
						updateTrigger = false;
						this.attack(this, item);
						break;
					}
					else if(item.type === GameObject.ROOM_TRIGGER) {
						updateTrigger = true;
					}
				}
			}

			if(canMove)
			{
				this.prevGridX = nextGridX;
				this.prevGridY = nextGridY;
				var newX = nextGridX * Terrain.Cfg.grid.width;
				var newY = nextGridY * Terrain.Cfg.grid.height;
				this.move(newX, newY);
				gCurrentCamera.update();
			}

			if(updateTrigger) {
				this.updateTrigger();
			}
		}

		SendSignal("Turn", 0, this);
	},


	//
	cellInfo: null,
	prevGridX: 0,
	prevGridY: 0
});