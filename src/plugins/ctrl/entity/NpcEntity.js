"use strict";

Entity.Npc = Entity.Character.extend
({
	activate: function()
	{
		this._super();

		this.pathfindingInfo = new AI.PathfindingInfo(0, 0, 0, 0);

		var self = this;
		SubscribeChannel(this, "Turn", function(event, data) {
			self.handleSignal_Turn(event, data);
		});
	},

	remove: function()
	{
		this._super();

		UnsubscribeChannel(this, "Turn");
	},


	handleSignal_Turn: function(event, data)
	{
		this.pathfindingInfo.startX = this.gridX;
		this.pathfindingInfo.startY = this.gridY;
		this.pathfindingInfo.endX = data.gridX;
		this.pathfindingInfo.endY = data.gridY;

		var path = AskData("Pathfinding", Pathfinding.Event.GET_PATH, this.pathfindingInfo);
		var pathLength = path.length;

		if(pathLength > 1)
		{
			var pickNode = path[pathLength-1];

			var newX = pickNode.x * Terrain.Cfg.grid.width;
			var newY = pickNode.y * Terrain.Cfg.grid.height;
			this.move(newX, newY);
		}
		else {
			this.attack(this, data);
		}
	},

	die: function()
	{
		mighty.Macro.CreateEntity("blood_decal", this.x, this.y);
		this.remove();
	},


	//
	pathfindingInfo: null
});