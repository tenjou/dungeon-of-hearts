Entity.Character = Entity.Geometry.extend
({
	activate: function()
	{
		this._super();

		this.stats = new Ctrl.CharacterStats();
	},

	attack: function(src, target)
	{
		var attackInfo = Mechanics.AttackInfo;
		attackInfo.src = src;
		attackInfo.target = target;
		SendSignal("Mechanics", Mechanics.Event.ATTACK, attackInfo);
	},

	doDmg: function(value)
	{
		this.stats.health += value; console.log("health: ", this.stats.health);
		if(this.stats.health <= 0) {
			this.die();
		}
	},

	die: function() {
		console.log("died");
	},

	//
	stats: null
});