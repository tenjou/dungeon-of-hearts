Define("GameObject", "", [
	"PLAYER", "ROOM_TRIGGER", "WALL", "NPC", "NPC_SPAWN"
]);

Define("Direction", "", [
	"LEFT", "RIGHT", "UP", "DOWN", "NONE"
]);

Define("Entity", "Type", [
	"PLAYER", "NPC", "NPC_SPAWN", "CHARACTER"
]);

DefineObj("Entity", {
	PLAYER: "Player",
	NPC: "Npc",
	NPC_SPAWN: "NpcSpawn",
	CHARACTER: "Character"
});

DefineObjData("Entity", {
	PLAYER: {
		extend: "CHARACTER"
	},
	NPC: {
		extend: "CHARACTER"
	},
	NPC_SPAWN: {
		extend: "CHARACTER"
	},
	CHARACTER: {
		extend: "GEOMETRY"
	}
});

Define("Brush", "Type", [
	"BLOOD_DECAL"
]);

DefineObj("Brush", {
	BLOOD_DECAL: "BloodDecal"
});

DefineObjData("Brush", {
	BLOOD_DECAL: {
		"extend": "BASIC"
	}
});

DefineStages({
	BLOOD_DECAL: [ "1", "2" ]
});