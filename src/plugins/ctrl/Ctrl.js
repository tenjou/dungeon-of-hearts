Ctrl.Manager = Plugin.extend
({
	install: function()
	{
		var self = this;
		SubscribeChannel(this, "Input", function(event, data) {
			self.handleSignal_Input(event, data);
		});
	},

	load: function()
	{
		// Generate dungeon.
		var sizeX = Terrain.Cfg.sizeX;
		var sizeY = Terrain.Cfg.sizeY;

		var genInfo = new DungeonGenerator.Info(sizeX, sizeY);
		var mapBuffer = AskData("DungeonGenerator", DungeonGenerator.Event.GENERATE, genInfo);
		SendSignal("DungeonGenerator", DungeonGenerator.Event.GENERATE_HD, null);

		var gridBuffer = new Entity.GridBuffer(mapBuffer, 0, 0, sizeX, sizeY);
		SendSignal("EntityCtrl", Entity.Event.LOAD_GRID_BUFFER, gridBuffer);

		// PLAYER
		this.player = AskData("DungeonGenerator", DungeonGenerator.Event.SPAWN_PLAYER, null);
		this.scene.camera.setFollowEntity(this.player);

		this.playerStats = new this.scope.CharacterStats();
		this.player.stats = this.playerStats;
		this.updateHud();
	},

	updateHud: function()
	{
		mighty.Templates.hud.js.setStage(this.stage);
		mighty.Templates.hud.js.setHealth(this.playerStats.health, this.playerStats.maxHealth);
		mighty.Templates.hud.js.setSkill(this.playerStats.skill, this.playerStats.maxSkill, this.playerStats.skillLevel);
	},


	handleSignal_Input: function(event, data)
	{
		if(!this.player) { return; }

		switch(event)
		{
			case Input.Event.KEY_DOWN:
			{
				switch(data.keyCode)
				{
					case Input.Key.ARROW_LEFT:
					case Input.Key.A:
						this.player.doTurn(Direction.LEFT);
						break;

					case Input.Key.ARROW_RIGHT:
					case Input.Key.D:
						this.player.doTurn(Direction.RIGHT);
						break;

					case Input.Key.ARROW_UP:
					case Input.Key.W:
						this.player.doTurn(Direction.UP);
						break;

					case Input.Key.ARROW_DOWN:
					case Input.Key.S:
						this.player.doTurn(Direction.DOWN);
						break;

					case 32: // Space
						this.player.doTurn(Direction.NONE);
						break;
				}
			} break;
		}
	},


	//
	player: null,
	playerStats: null,

	stage: 1
});

Ctrl.CharacterStats = function()
{
	this.skill = 0;
	this.maxSkill = 10;
	this.skillLevel = 1;

	this.health = 20;
	this.maxHealth = 20;
	this.attack = 5;
	this.defence = 10;
	this.critical = 50;
	this.evasion = 0;
	this.accuracy = 0;
};