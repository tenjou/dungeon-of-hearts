Npc.Manager = Plugin.extend
({
	install: function()
	{
		this.npcs = [];

		var self = this;
		SubscribeChannel(this, "Npc", function(event, data) {
			self.handleSignal_Npc(event, data);
		});
	},

	clear: function() {
		this.npcs.length = 0;
	},

	spawnNpc: function(spawn)
	{
		var npcX = spawn.gridX * Terrain.Cfg.grid.width;
		var npcY = spawn.gridY * Terrain.Cfg.grid.height;
		var npc = mighty.Macro.CreateEntity("npc", npcX, npcY);
		this.npcs.push(npc);
	},

	handleSignal_Npc: function(event, data)
	{
		switch(event)
		{
			case Npc.Event.SPAWN:
				this.spawnNpc(data);
				break;
		}
	},


	//
	npcs: null
});