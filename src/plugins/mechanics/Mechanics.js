Mechanics.Manager = Plugin.extend
({
	install: function()
	{
		var self = this;
		SubscribeChannel(this, "Mechanics", function(event, data) {
			self.handleSignal_Mechanics(event, data);
		});
	},

	doAttack: function(data)
	{
		var scrollingTextInfo = ScrollingText.Info;
		var src = data.src.stats;
		var target = data.target.stats;

		var chanceToMiss = target.evasion - src.accuracy;
		if(chanceToMiss < 0) { chanceToMiss = 0; }
		if(Random.getNumber(1, 100) <= chanceToMiss)
		{
			scrollingTextInfo.text = "Miss";
			scrollingTextInfo.x = data.target.x + gCurrentCamera.x + Random.getNumber(-10, 10);
			scrollingTextInfo.y = data.target.y + gCurrentCamera.y - 50;
			SendSignal("ScrollingText", ScrollingText.Event.ADD, scrollingTextInfo);
		}
		else
		{
			var defence = target.defence - Random.getNumber(0, 5); // Percents protection.
			var attack = -Math.ceil(src.attack - ((src.attack / 100) * defence));

			if(Random.getNumber(1, 100) < src.critical)
			{
				attack *= 2;
				scrollingTextInfo.text = attack;
				scrollingTextInfo.x = data.target.x + gCurrentCamera.x + Random.getNumber(-10, 10);
				scrollingTextInfo.y = data.target.y + gCurrentCamera.y - 50;
				SendSignal("ScrollingText", ScrollingText.Event.ADD_CRITICAL, scrollingTextInfo);
			}
			else
			{
				scrollingTextInfo.text = attack;
				scrollingTextInfo.x = data.target.x + gCurrentCamera.x + Random.getNumber(-10, 10);
				scrollingTextInfo.y = data.target.y + gCurrentCamera.y - 50;
				SendSignal("ScrollingText", ScrollingText.Event.ADD, scrollingTextInfo);
			}

			data.target.doDmg(attack);
		}
	},

	handleSignal_Mechanics: function(event, data)
	{
		switch(event)
		{
			case Mechanics.Event.ATTACK:
				this.doAttack(data);
				break
		}
	}
});

Mechanics.AttackInfo = {
	src: null,
	target: null
};
