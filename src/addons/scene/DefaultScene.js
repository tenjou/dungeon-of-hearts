Scene.Default = Scene.Base.extend
({
	setup: function()
	{
		this.loadLevel(Palettes.Map.getByID(Engine.Cfg.defaultLevel));

		this._super();
	}
});