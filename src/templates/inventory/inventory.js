mighty.TemplateJS.inventory = function()
{
	this.load = function()
	{
		var self = this;
		$(".close_inventory").bind("click", function() {
			mighty.Templates.inventory.hide();
		});
	};

	this.clear = function() {
		$(".close_inventory").unbind("click");
	};
};