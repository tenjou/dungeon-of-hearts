mighty.TemplateJS.loading = function()
{
	this.load = function()
	{
		mighty.Loader.loadingTemplate = mighty.Templates.loading;
		mighty.Loader.loadingPercent = $("#loading_scr .progress_block .percent");
		mighty.Loader.loadingBar = $("#loading_scr .progress_block .main_content .progressbar .bar");
		mighty.Loader.loadingLabel = $("#loading_scr .progress_block .main_content .main_text");
		mighty.Loader.initLoading();
		mighty.Loader.showLoading(true);
	};
};