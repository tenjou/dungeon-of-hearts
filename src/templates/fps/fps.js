mighty.TemplateJS.fps = function()
{
	this.init = function()
	{
		var self = this;
		SubscribeChannel(this, "Scene", function(event, obj) { return self.handleSignal_Scene(event, obj); });
	};


	this.handleSignal_Scene = function(event, obj)
	{
		switch(event)
		{
			case Scene.Event.FPS:
				this.counter("FPS: " + obj);
				return true;

			case Scene.Event.TIME_FRAME:
				this.tFrame("tFrame: " + obj.toFixed(3) + "ms");
				return true;
		}

		return false;
	};

	//
	this.counter = ko.observable("FPS: 0");
	this.tFrame = ko.observable("tFrame: 0ms");
};