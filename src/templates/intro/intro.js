mighty.TemplateJS.intro = function()
{
	this.load = function()
	{
		var self = this;
		SubscribeChannel(this, "Scene", function(event, data) {
			self.handleSignal_Scene(event, data);
		});
	};


	this.handleSignal_Scene = function(event, data)
	{
		switch(event)
		{
			case Scene.Event.LOADED:
				this.fadeAway();
				break;
		}
	}

	this.fadeAway = function()
	{
		Ctrl.plugin.addTimer(function()
		{
			$(".intro_scr").fadeOut(1000, function() {
				mighty.Templates.intro.hide();
			});
		}, 2000, 1);
	}
};