mighty.TemplateJS.hud = function()
{
	this.load = function()
	{
		this.stage = $(".stage_number");

		this.healthStatus = $(".health_bar .text");
		this.healthBar = $(".health_bar .progress");

		this.skillStatus = $(".secondary_bar .text");
		this.skillBar = $(".secondary_bar .progress");
		this.skillIcon = $(".secondary_bar .icon");

		$(".inventory_show").bind("click", function() {
			mighty.Templates.inventory.show();
		});
	};

	this.clear = function()  {
		$(".inventory_show").unbind("click");
	};

	this.setStage = function(stage) {
		this.stage.html("Stage " + stage);
	};

	this.setHealth = function(value, maxValue)
	{
		var percents = (100 / maxValue) * value;
		if(isNaN(percents)) {
			percents = 0;
		}

		this.healthStatus.html(value + "/" + maxValue);
		this.healthBar.css("width", percents + "%");
	};

	this.setSkill = function(value, maxValue, level)
	{
		var percents = (100 / maxValue) * value;
		if(isNaN(percents)) {
			percents = 0;
		}

		this.skillStatus.html(value + "/" + maxValue);
		this.skillBar.css("width", percents + "%");
		this.skillIcon.html(level);
	};


	//
	this.healthStatus = null;
	this.healthBar = null;

	this.skillStatus = null;
	this.skillBar = null;
	this.skillIcon = null;

	this.inventoryButton = null;
};