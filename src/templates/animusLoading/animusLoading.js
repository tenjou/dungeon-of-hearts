mighty.TemplateJS.animusLoading = function()
{
	this.load = function()
	{
		mighty.Loader.loadingTemplate = mighty.Templates.animusLoading;
		mighty.Loader.loadingBar = $(".loading_scr .progress_bar .progress");
		mighty.Loader.initLoading();
		mighty.Loader.showLoading(true);
	};
};