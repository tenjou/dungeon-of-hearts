mighty.TemplateJS.scrollingText = function()
{
	this.init = function()
	{
		this.scrollingElement = $('<div class="dmg_number float_up"></div>');
		this.scrollingElementCritical = $('<div class="critical_strike shake_furiously"></div>');
	};

	this.load = function()
	{

	};

	this.clear = function()
	{
		var node = mighty.Templates.scrollingText.element;

		while(node.hasChildNodes()) {
			node.removeChild(node.lastChild);
		}
	};


	this.add = function(text, x, y)
	{
		var textElement = this.scrollingElement.clone();
		textElement.html(text);
		textElement.css("top", y + "px");
		textElement.css("left", x + "px");
		mighty.Templates.scrollingText.element.appendChild(textElement[0]);

		return textElement[0];
	};

	this.addCritical = function(x, y)
	{
		var textElement = this.scrollingElementCritical.clone();
		textElement.css("top", y + "px");
		textElement.css("left", x + "px");
		mighty.Templates.scrollingText.element.appendChild(textElement[0]);

		return textElement[0];
	};

	this.remove = function(element) {
		mighty.Templates.scrollingText.element.removeChild(element);
	};


	//
	this.scrollingElement = null;
	this.scrollingElementCritical = null;
};