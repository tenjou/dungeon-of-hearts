function main()
{
	Engine.initUI("view");
	mighty.Template.init();
	mighty.Template.create("scrollingText").show();
	mighty.Template.create("hud").show();
	mighty.Template.create("inventory");
	//mighty.Template.create("intro").show();
	mighty.Template.create("animusLoading").show();
	//mighty.Template.create("fps").show();

	mighty.Loader.load(mainLoad);
}

function mainLoad()
{
	Engine.init();

	var sceneMgr = new Scene.Manager();

	var defaultScene = new Scene.Default();
	sceneMgr.setCurrentScene(defaultScene);
	sceneMgr.update();
}
window.onload = main;