(mighty.Loader.path = (window.gParams.gPath ? window.gParams.gPath : ""));(function(){
var loader = mighty.Loader;
loader.include("src/plugins/AI/AI.js");
loader.include("src/plugins/AI/modules/pathfinding/AStar/AStar.js");
loader.include("src/plugins/AI/modules/pathfinding/AStar/AStarBuffer.js");
loader.include("src/plugins/AI/modules/pathfinding/AStar/AStarNode.js");
loader.include("src/plugins/AI/modules/pathfinding/Heuristic.js");
loader.include("src/plugins/AI/modules/pathfinding/Pathfinding.js");
loader.include("src/plugins/ctrl/Ctrl.js");
loader.include("src/plugins/ctrl/entity/NpcEntity.js");
loader.include("src/plugins/ctrl/entity/PlayerEntity.js");
loader.include("src/plugins/dungeonGenerator/DungeonGenerator.js");
loader.include("src/plugins/fogOfWar/FogOfWar.js");
loader.include("src/addons/scene/DefaultScene.js");
})();