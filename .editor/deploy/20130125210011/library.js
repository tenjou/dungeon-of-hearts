gParams.rel=(gParams.branch ? 0 : 1);
var ActionType = window.ActionType = {
	"NONE":0,
	"ADD":1,
	"REMOVE":2,
	"MOVE":3,
	"LOADED":4,
	"UPDATE":5,
	"MOUSE_CLICK":6,
	"MOUSE_OVER":7,
	"UPDATE ":8
};
var Brush = window.Brush = {
	"Type":{
		"BASIC":0,
		"TERRAIN":1,
		"TILE":2
	},
	"Obj":[
		"Basic",
		"Terrain",
		"Tile"
	],
	"Event":{
		"LOADED":0
	},
	"Shape":{
		"BOX":0,
		"SPHERE":1,
		"TRIANGLE":2
	},
	"Stage":{
		"DEFAULT":0,
		"TOP_DOWN":1,
		"ISOMETRIC":2
	},
	"Cfg":{
		"name":"brush",
		"isTemplate":0,
		"enabled":1,
		"origin":"core",
		"id":1
	},
	"palette":[
		{
			"name":"null",
			"type":2,
			"texture":-2,
			"head":{
				"offsetX":0,
				"offsetY":0,
				"gridSizeX":1,
				"gridSizeY":1
			},
			"footer":{
				"stages":[
					{
						"type":1,
						"texture":-2,
						"options":{
							"flip":"NONE",
							"isFull":0
						}
					},
					{
						"type":2,
						"texture":-3,
						"options":{
							"flip":"NONE",
							"isFull":0
						}
					}
				]
			},
			"body":{
				"type":0,
				"layerIndex":0,
				"fillType":0,
				"isModifier":1,
				"isUpdatePreview":1
			},
			"id":-4
		},
		{
			"name":"floor",
			"type":2,
			"texture":23,
			"head":{
				"offsetX":0,
				"offsetY":0,
				"gridSizeX":1,
				"gridSizeY":1
			},
			"footer":{
				"stages":[
					{
						"type":0,
						"texture":23,
						"options":{
							"flip":0,
							"isFull":0
						}
					}
				]
			},
			"body":{
				"type":"UNKNOWN",
				"layerIndex":0,
				"fillType":"DEFAULT",
				"isModifier":1,
				"isUpdatePreview":1
			},
			"id":24
		},
		{
			"name":"ground",
			"type":0,
			"texture":37,
			"head":{
				"offsetX":0,
				"offsetY":0,
				"gridSizeX":1,
				"gridSizeY":1
			},
			"id":26
		},
		{
			"name":"stoneWall",
			"type":0,
			"texture":28,
			"head":{
				"offsetX":0,
				"offsetY":0,
				"gridSizeX":1,
				"gridSizeY":1
			},
			"id":29
		},
		{
			"name":"debug",
			"type":0,
			"texture":31,
			"head":{
				"offsetX":0,
				"offsetY":0,
				"gridSizeX":1,
				"gridSizeY":1
			},
			"id":32
		},
		{
			"name":"wall",
			"type":0,
			"texture":22,
			"head":{
				"offsetX":0,
				"offsetY":0,
				"gridSizeX":1,
				"gridSizeY":1
			},
			"id":35
		},
		{
			"name":"wall_2",
			"type":0,
			"texture":39,
			"head":{
				"offsetX":0,
				"offsetY":0,
				"gridSizeX":1,
				"gridSizeY":1
			},
			"id":40
		},
		{
			"name":"player",
			"type":0,
			"texture":43,
			"head":{
				"offsetX":0,
				"offsetY":0,
				"gridSizeX":1,
				"gridSizeY":1
			},
			"id":44
		}
	]
};
var Camera = window.Camera = {
	"Event":{
		"UNKNOWN":0,
		"MOVED":1
	},
	"Position":{
		"DEFAULT":0,
		"CENTER":1,
		"H_CENTER":2,
		"V_CENTER":3,
		"FOLLOW":4
	},
	"Cfg":{
		"name":"camera",
		"enabled":1,
		"isTemplate":1,
		"origin":"core",
		"position":{
			"isDrag":1,
			"ignoreBorder":1,
			"type":1
		},
		"zoom":{
			"active":"true",
			"useAutoZoom":0,
			"min":0.6,
			"max":1.4,
			"default":1,
			"step":0.1
		},
		"id":2
	}
};
var Component = window.Component = {
	"Type":{
		"BASIC":0
	},
	"Obj":[
		"Basic"
	],
	"Cfg":{
		"name":"component",
		"enabled":1,
		"isTemplate":1,
		"origin":"core",
		"id":3
	}
};
var Editor = window.Editor = {
	"Event":{
		"UNKNOWN":0,
		"USE_PLUGIN":1,
		"USE_ITEM":2,
		"USE_ITEM_ID":3,
		"SET_MODE":4
	},
	"Mode":{
		"NONE":0,
		"ADD":1,
		"REMOVE":2,
		"MOVE":3
	},
	"Cfg":{
		"name":"editor",
		"useAvailable":0,
		"enabled":1,
		"isTemplate":1,
		"origin":"core",
		"id":5
	}
};
var Engine = window.Engine = {
	"Event":{
		"UNKNOWN":0,
		"CAMERA":1,
		"ZOOM":2,
		"RESIZE":3,
		"FOCUS":4
	},
	"Layer":{
		"STATIC":0,
		"DYNAMIC":1
	},
	"Cfg":{
		"name":"engine",
		"offlineMode":0,
		"tUpdateDelta":33.3,
		"tSleep":16.6,
		"isDebug":1,
		"serverPath":"src\/server",
		"defaultLevel":38,
		"enabled":1,
		"isTemplate":1,
		"origin":"core",
		"id":6
	}
};
var Loader = window.Loader = {
	"Event":{
		"ADD_ELEMENT":0,
		"GET_ELEMENT":1,
		"UPDATE_ELEMENT":2
	}
};
var GameObject = window.GameObject = {
	"UNKNOWN":0,
	"PLAYER":1
};
var Priority = window.Priority = {
	"VERY_HIGH":2000,
	"HIGH":1000,
	"MEDIUM":500,
	"LOW":0
};
var Entity = window.Entity = {
	"Type":{
		"BASIC":0,
		"GEOMETRY":1,
		"ROTATE":2
	},
	"Obj":[
		"Base",
		"Geometry",
		"Rotate"
	],
	"Event":{
		"NONE":0,
		"ADD":1,
		"REMOVE":2,
		"ADDED":3,
		"REMOVED":4,
		"ADD_FROM_INFO":5,
		"OVER":6,
		"NOT_OVER":7,
		"PRESSED":8,
		"CLICKED":9,
		"DRAGGED":10,
		"GET_BY_TYPE":11,
		"LOAD_GRID_BUFFER":12
	},
	"VolumeType":{
		"NONE":0,
		"AABB":1,
		"SPHERE":2
	},
	"RENDER_METHOD":{
		"DEFAULT":0,
		"DISCARD":1
	},
	"Cfg":{
		"name":"entity",
		"pixelPerfect":0,
		"enabled":1,
		"isTemplate":1,
		"origin":"core",
		"render":{
			"method":0
		},
		"id":7
	},
	"palette":[
		{
			"name":"ground",
			"type":1,
			"brush":26,
			"body":{
				"isSaved":1,
				"type":0,
				"depthIndex":0,
				"isVisible":1,
				"isPaused":0
			},
			"id":27
		},
		{
			"name":"stoneWall",
			"type":1,
			"brush":29,
			"body":{
				"isSaved":1,
				"type":0,
				"depthIndex":0,
				"isVisible":1,
				"isPaused":0
			},
			"id":30
		},
		{
			"name":"debug",
			"type":1,
			"brush":32,
			"body":{
				"isSaved":1,
				"type":0,
				"depthIndex":0,
				"isVisible":1,
				"isPaused":0
			},
			"id":33
		},
		{
			"name":"wall",
			"type":1,
			"brush":35,
			"body":{
				"isSaved":1,
				"type":0,
				"depthIndex":0,
				"isVisible":1,
				"isPaused":0
			},
			"id":36
		},
		{
			"name":"player",
			"type":1,
			"brush":44,
			"body":{
				"isSaved":1,
				"type":1,
				"depthIndex":0,
				"isVisible":1,
				"isPaused":0
			},
			"id":45
		}
	]
};
var Input = window.Input = {
	"Key":{
		"A":65,
		"D":68,
		"S":83,
		"W":87,
		"NUM_0":48,
		"NUM_1":49,
		"NUM_2":50,
		"NUM_3":51,
		"NUM_4":52,
		"NUM_5":53,
		"NUM_6":54,
		"NUM_7":55,
		"NUM_8":56,
		"NUM_9":57,
		"PLUS":187,
		"MINUS":189,
		"ARROW_LEFT":37,
		"ARROW_UP":38,
		"ARROW_RIGHT":39,
		"ARROW_DOWN":40,
		"BUTTON_LEFT":0,
		"BUTTON_MIDDLE":1,
		"BUTTON_RIGHT":2
	},
	"Event":{
		"UNKNOWN":0,
		"MOVED":1,
		"INPUT_DOWN":2,
		"INPUT_UP":3,
		"KEY_DOWN":4,
		"KEY_UP":5,
		"CLICKED":6,
		"DB_CLICKED":7,
		"PINCH_IN":8,
		"PINCH_OUT":9,
		"IS_KEY":10
	},
	"Cfg":{
		"name":"input",
		"stickyKeys":1,
		"isDebug":0,
		"enabled":1,
		"isTemplate":1,
		"origin":"core",
		"id":8
	}
};
var Patch = window.Patch = {
	"Type":{
		"TOP_DOWN":0,
		"ISOMETRIC":1
	},
	"Obj":[
		"TopDown",
		"Isometric"
	],
	"Event":{
		"VISIBLE_PATCHES":0
	},
	"Cfg":{
		"name":"patch",
		"optimalSizeX":512,
		"optimalSizeY":512,
		"isDebug":1,
		"enabled":1,
		"isTemplate":1,
		"origin":"core",
		"id":10
	}
};
var Grid = window.Grid = {
	"Event":{
		"IS_CELL_FULL":0,
		"GET_CELL":1,
		"GET_RANDOM_CELL":2
	}
};
var Resource = window.Resource = {
	"Type":{
		"TEXTURE":0,
		"ANIM_TEXTURE":1,
		"SOUND":2,
		"UNKNOWN":3
	},
	"Flip":{
		"NONE":0,
		"HORIZONTAL":1,
		"VERTICAL":2,
		"HORIZONTAL_VERTICAL":3
	},
	"Event":{
		"UNKNOWN":0,
		"LOADED":1,
		"REPLACE":2
	},
	"Obj":[
		"Texture",
		"AnimTexture",
		"Sound",
		"Resource"
	],
	"ModuleType":{
		"TEXTURE":"Texture",
		"ANIM_TEXTURE":"Texture",
		"SOUND":"Sound"
	},
	"Cfg":{
		"name":"resource",
		"path":"assets\/deploy",
		"enabled":1,
		"isTemplate":1,
		"origin":"core",
		"id":12
	},
	"Templates":{
		
	},
	"palette":[
		{
			"name":"null_iso",
			"type":0,
			"fps":9,
			"numFrames":15,
			"img":"-3.png",
			"p":"",
			"id":-3,
			"ext":"png",
			"width":90,
			"height":45,
			"date":1358968986
		},
		{
			"name":"null",
			"type":0,
			"fps":9,
			"numFrames":15,
			"img":"-2.png",
			"p":"",
			"id":-2,
			"ext":"png",
			"width":64,
			"height":64,
			"date":1358968986
		},
		{
			"name":"wall",
			"type":0,
			"fps":0,
			"numFrames":1,
			"img":"22.png",
			"p":"",
			"id":22,
			"ext":"png",
			"width":16,
			"height":16,
			"date":1359034030
		},
		{
			"name":"floor",
			"type":0,
			"fps":0,
			"numFrames":1,
			"img":"23.png",
			"p":"",
			"id":23,
			"ext":"png",
			"width":16,
			"height":16,
			"date":1358971784
		},
		{
			"name":"stoneWall",
			"type":0,
			"fps":0,
			"numFrames":1,
			"img":"28.png",
			"p":"",
			"id":28,
			"ext":"png",
			"width":16,
			"height":16,
			"date":1358973627
		},
		{
			"name":"debug",
			"type":0,
			"fps":0,
			"numFrames":1,
			"img":"31.png",
			"p":"",
			"id":31,
			"ext":"png",
			"width":16,
			"height":16,
			"date":1359025836
		},
		{
			"name":"ground",
			"type":0,
			"fps":0,
			"numFrames":1,
			"img":"37.png",
			"p":"",
			"id":37,
			"ext":"png",
			"width":16,
			"height":16,
			"date":1359033294
		},
		{
			"name":"wall_2",
			"type":0,
			"fps":0,
			"numFrames":1,
			"img":"39.png",
			"p":"",
			"id":39,
			"ext":"png",
			"width":32,
			"height":64,
			"date":1359040120
		},
		{
			"name":"player",
			"type":0,
			"fps":0,
			"numFrames":1,
			"img":"43.png",
			"p":"",
			"id":43,
			"ext":"png",
			"width":16,
			"height":16,
			"date":1359140343
		}
	]
};
var MaskType = window.MaskType = {
	"DEFAULT":0,
	"CENTER":1,
	"CONTINUOUS":2
};
var Scene = window.Scene = {
	"Event":{
		"LOADED":0,
		"LOAD_LEVEL":1,
		"LOAD_LEVEL_ID":2,
		"FPS":3,
		"TIME_FRAME":4,
		"GET_LEVEL":5,
		"SET_PAUSE":6
	},
	"Cfg":{
		"name":"scene",
		"enabled":1,
		"isTemplate":1,
		"origin":"core",
		"id":13
	}
};
var Terrain = window.Terrain = {
	"Type":{
		"TOP_DOWN":0,
		"ISOMETRIC":1
	},
	"Event":{
		"NONE":0,
		"ADD":1,
		"REMOVE":2,
		"MOVE":3,
		"UPDATE":4,
		"LOADED":5,
		"TILE_CLICKED":6,
		"INTERACT":7,
		"INTERACT_DONE":8,
		"DATA":9
	},
	"StatusType":{
		"UNKNOWN":0,
		"NO_CHANGES":1,
		"NO_LAYER":2,
		"ADDED":3,
		"CHANGED":4
	},
	"LayerType":{
		"DEFAULT":0,
		"TEMPORARY":1,
		"WITH_TEMPORARY":2
	},
	"FillType":{
		"DEFAULT":0,
		"FLOOD":1
	},
	"Status":{
		"NO_CHANGES":0,
		"NO_LAYER":1,
		"ADDED":2,
		"CHANGED":3
	},
	"Cfg":{
		"name":"terrain",
		"visible":1,
		"type":0,
		"tileWidth":16,
		"tileHeight":16,
		"tileDepth":1,
		"offlineMode":0,
		"enabled":1,
		"isTemplate":1,
		"origin":"core",
		"grid":{
			"use":0,
			"width":16,
			"height":16,
			"showDebug":0
		},
		"id":15
	}
};
var DungeonGenerator = window.DungeonGenerator = {
	"Event":{
		"GENERATE":0
	},
	"Cfg":{
		"name":"dungeonGenerator",
		"enabled":1,
		"isTemplate":1,
		"origin":"project",
		"id":21
	}
};
var Cursor = window.Cursor = {
	"Cfg":{
		"name":"cursor",
		"stepSizeX":16,
		"stepSizeY":16,
		"enabled":1,
		"isTemplate":1,
		"origin":"core",
		"id":4
	}
};
var Map = window.Map = {
	"Cfg":{
		"name":"map",
		"enabled":1,
		"isTemplate":1,
		"origin":"core",
		"id":9
	},
	"palette":[
		{
			"name":"default",
			"gridX":10,
			"gridY":10,
			"preview":0,
			"defaultBrush":-4,
			"id":-5
		},
		{
			"name":"level_1",
			"gridX":32,
			"gridY":32,
			"preview":0,
			"defaultBrush":24,
			"id":25
		},
		{
			"name":"level_2",
			"gridX":100,
			"gridY":100,
			"preview":0,
			"defaultBrush":24,
			"id":34
		},
		{
			"name":"level 3",
			"gridX":50,
			"gridY":50,
			"preview":0,
			"defaultBrush":24,
			"id":38
		}
	]
};
var Plugin = window.Plugin = {
	"palette":[
		"DungeonGenerator",
		"Ctrl"
	],
	"Cfg":{
		"name":"plugin",
		"enabled":1,
		"isTemplate":1,
		"origin":"core",
		"id":11
	}
};
var Template = window.Template = {
	"Cfg":{
		"name":"template",
		"enabled":1,
		"isTemplate":1,
		"origin":"core",
		"id":14
	}
};
var Ctrl = window.Ctrl = {
	"Cfg":{
		"name":"ctrl",
		"enabled":1,
		"isTemplate":1,
		"origin":"project",
		"id":42
	}
};